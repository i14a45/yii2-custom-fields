<?php


namespace i14a45\customfields\behaviors;

use Yii;
use i14a45\customfields\models\CustomField;
use i14a45\customfields\models\CustomFieldValue;
use i14a45\customfields\Module;
use yii\base\Behavior;
use yii\base\DynamicModel;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\validators\Validator;

/**
 * Class CustomFieldsBehavior
 *
 * @property ActiveRecord $owner
 */
class CustomFieldsBehavior extends Behavior
{
    /**
     * @var CustomField[]
     */
    protected $fields = [];

    /**
     * @var DynamicModel
     */
    protected $model;

    /**
     * {@inheritDoc}
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * @param ModelEvent $event
     * @return bool
     */
    public function beforeValidate(ModelEvent $event)
    {
        $this->getCustomModel();
        if ($this->model->load(Yii::$app->request->post()) && $this->model->validate()) {
            return true;
        }

        $event->isValid = false;
        return $event->isValid;
    }

    public function afterSave()
    {
        $model = $this->getCustomModel();

        $values = $this->model->getAttributes();

        $valueModels = CustomFieldValue::findByFieldIds(array_keys($this->fields), $this->owner->getPrimaryKey());

        foreach ($this->fields as $field) {
            $valueModel = ArrayHelper::getValue($valueModels, $field->id);
            if (empty($valueModel)) {
                $valueModel = new CustomFieldValue();
                $valueModel->field_id = $field->id;
                $valueModel->model_pk = $this->owner->getPrimaryKey();
            }
            $valueModel->value = ArrayHelper::getValue($values, $field->getAttributeName());
            $valueModel->save();
        }
    }

    public function afterDelete()
    {
        CustomFieldValue::deleteByFieldIds(array_keys($this->fields), $this->owner->getPrimaryKey());
    }

    /**
     * @return DynamicModel
     */
    public function getCustomModel()
    {
        if (!$this->model) {
            $this->model = $this->createCustomModel();
        }
        return $this->model;
    }

    /**
     * @param \yii\base\Component $owner
     */
    public function attach($owner)
    {
        $module = Yii::$app->getModule('custom-fields');

        $ownerClass = get_class($owner);

        $modelClasses = ArrayHelper::map($module->getModels(), 'class', 'class');
        if (!in_array($ownerClass,$modelClasses)) {
            return;
        }

        parent::attach($owner);

        $this->fields = CustomField::findByModelClass(get_class($this->owner));
    }

    /**
     * @return DynamicModel
     */
    protected function createCustomModel()
    {
        $values = CustomFieldValue::findByFieldIds(array_keys($this->fields), $this->owner->getPrimaryKey());

        $attributeNames = [];
        $this->model = new DynamicModel();
        foreach ($this->fields as $field) {
            $attributeNames[] = $field->getAttributeName();
            $this->model->defineAttribute($field->getAttributeName(), ArrayHelper::getValue($values, "{$field->id}.value"));
            $this->model->addRule($field->getAttributeName(), $this->getValidator($field));
            if ($field->required) {
                $this->model->addRule($field->getAttributeName(), 'required');
            }
        }

        return $this->model;
    }

    /**
     * @param CustomField $field
     * @return Validator
     */
    protected function getValidator($field)
    {
        $params = [];
        switch ($field->type) {
            default:
                $type = 'string';
                $params = ['max' => 2555];
        }

        return Validator::createValidator($type, $this->model, $field->getAttributeName(), $params);
    }
}