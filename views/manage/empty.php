<?php

/* @var \yii\web\View $this */

$this->title = 'Произвольные поля не настроены';
?>

<?= \yii\bootstrap4\Alert::widget([
    'options' => [
        'class' => 'alert-warning',
    ],
    'body' => '<i class="icon fas fa-exclamation-triangle"></i>' . $this->title,
    'closeButton' => false,
]) ?>
