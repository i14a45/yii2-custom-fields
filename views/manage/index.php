<?php

use yii\bootstrap4\Dropdown;
use yii\data\ArrayDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var array $entity */
/* @var array $entityList */
/* @var ArrayDataProvider $dataProvider */

$this->title = "Произвольные поля. Таблица {$entity['name']}";
$this->params['breadcrumbs'][] = $this->title;

$disabledDropdownBtn = count($entityList) === 1;
?>

<div class="card">
    <div class="card-header">
        <?= Html::a('Добавить поле', ['create', 'hash' => md5($entity['class'])], ['class' => 'btn btn-success']) ?>

        <div class="card-tools">
            <div class="btn-group">
                <?= Html::button($entity['name'], [
                    'class' => 'btn btn-primary dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'disabled' => $disabledDropdownBtn,
                ]) ?>
                <?= Dropdown::widget([
                    'items' => ArrayHelper::map($entityList, 'class', function($item) {
                        return ['label' => $item['name'], 'url' => ['index', 'hash' => md5($item['class'])]];
                    }),
                    'options' => [
                        'class' => 'dropdown-menu-right'
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <?= GridView::widget([
            'filterModel' => null,
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'label',
                'placeholder',
                'description',
                [
                    'class' => ActionColumn::class,
                    'template' => '{update} {delete}',
                    'buttons'  => [
                        'update' => function ($url) {
                            return Html::a('<span class="fa fa-pen"></span>', $url, [
                                'class' => 'btn btn-sm btn-info',
                            ]);
                        },
                        'delete' => function ($url) {
                            return Html::a('<span class="fa fa-trash"></span>', $url, [
                                'class' => 'btn btn-sm btn-danger',
                                'data-confirm' => 'Удалить?',
                                'data-method' => 'post',
                            ]);
                        },
                    ],
                ],
            ],
        ]) ?>
    </div>
</div>
