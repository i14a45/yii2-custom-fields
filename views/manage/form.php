<?php

use common\components\Constants;
use common\models\db\CustomField;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var array $entity */
/* @var CustomField $model */

$this->title = $model->isNewRecord ? 'Новое поле' : 'Редактировать поле "' . $model->label . '"';

$this->params['breadcrumbs'][] = [
    'label' => 'Произвольные поля. Таблица "' . $entity['name'] . '"',
    'url' => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'label')->textInput() ?>
                <?= $form->field($model, 'placeholder')->textInput() ?>
                <?= $form->field($model, 'hint')->textInput() ?>
                <?= $form->field($model, 'description')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', [
                        'class' => 'btn btn-success'
                    ]) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <!--<div class="col-md-1 col-sm-1"></div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="d-flex align-items-center h-100">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Образец поля</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label">Метка</label>
                                <input class="form-control" type="text" placeholder="Плейсхолдер"/>
                                <small class="form-text text-muted">Подсказка</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>
