Yii2 Custom Fields
=======================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).
```
composer require i14a45/yii2-custom-fields
```

Configuration
------------

**Database Migrations**

```
./yii migrate --migrationPath=@vendor/i14a45/yii2-custom-fields/migrations
```

**Module setup**

To access the module, you need to add the following code to your application configuration:
```php
return [
    //...
    'modules' => [
        //...
        'custom-fields' => [
            'class' => \i14a45\customfields\Module::class,
            'models' => [
                [
                    'class' => SampleModel::class,
                    'name' => 'Sample model',
                ],
            ],
        ],
    ],
];
```
>**NOTE:** Module id must be `custom-fields` and not otherwise
> 
**Behavior**
```php
use i14a45\customfields\behaviors\CustomFieldsBehavior;

class SampleModel extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'customFields' => [
                'class' => CustomFieldsBehavior::class,
            ]
        ];
    }
}
```
