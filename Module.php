<?php


namespace i14a45\customfields;

use i14a45\customfields\behaviors\CustomFieldsBehavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Module
 */
class Module extends \yii\base\Module
{
    /**
     * @var array
     */
    protected $models = [];

    /**
     * @var array
     */
    public $accessControlConfig = [
        'class' => AccessControl::class,
        'rules' => [
            [
                'allow'   => true,
                'roles'   => ['admin'],
            ],
        ],
    ];

    /**
     * @var array
     */
    public $verbsConfig = [
        'class'   => VerbFilter::class,
        'actions' => [
            'delete' => ['post'],
        ],
    ];
    
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'i14a45\customfields\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

    }

    /**
     * @return array
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * @param array $models
     */
    public function setModels(array $models)
    {
        $result = [];
        foreach ($models as $model) {
            if (!array_key_exists('class', $model)) {
                throw new InvalidConfigException('Empty model class');
            }
            if (!class_exists($model['class'])) {
                throw new InvalidConfigException("Model {$model['class']} not exists");
            }

            $result[md5($model['class'])] = $model;
        }
        $this->models = $result;
    }
}