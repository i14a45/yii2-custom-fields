<?php


namespace i14a45\customfields\widgets;


use i14a45\customfields\models\CustomField;
use i14a45\customfields\traits\CustomFieldsTrait;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

/**
 * CustomFieldsWidget
 */
class CustomFieldsInputWidget extends Widget
{
    /**
     * @var ActiveRecord|CustomFieldsTrait
     */
    public $model;

    /** @var ActiveForm */
    public $form;

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        $attributes = CustomField::findByModelClass(get_class($this->model));

        $html = '';
        $counter = 0;
//dump($this->model->getCustomModel());
        $customModel = $this->model->getCustomModel();
        foreach ($attributes as $attribute) {
            $field = $this->form->field($customModel, $attribute->getAttributeName())
                ->label($attribute->label)
                ->hint($attribute->hint);
            switch ($attribute->type) {
                default:
                    $html .= $field->textInput(['placeholder' => $attribute->placeholder]);
            }
        }
        return $html;
    }
}