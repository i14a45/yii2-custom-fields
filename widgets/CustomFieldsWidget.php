<?php


namespace i14a45\customfields\widgets;


use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

/**
 * Class CustomFieldsWidget
 */
class CustomFieldsWidget extends Widget
{
    /**
     * @var ActiveRecord
     */
    public $model;

    /** @var ActiveForm */
    public $form;

    public function run()
    {
        return '';
    }
}