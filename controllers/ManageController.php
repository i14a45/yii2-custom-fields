<?php


namespace i14a45\customfields\controllers;


use i14a45\customfields\models\CustomField;
use i14a45\customfields\Module;
use Yii;
use yii\base\Action;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ManageController
 * 
 * @property Module $module
 */
class ManageController extends Controller
{
    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        if (!empty($this->module->accessControlConfig)) {
            $behaviors['access'] = $this->module->accessControlConfig;
        }        
        if (!empty($this->module->verbsConfig)) {
            $behaviors['verbs'] = $this->module->verbsConfig;
        }
        return $behaviors;
    }

    /**
     * @param Action $action
     * @return false
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        /** @var Module $module */
        $module = $this->module;

        if (empty($module->models) && $action->id !== 'empty') {
            Yii::$app->response->redirect(["{$module->id}/{$this->id}/empty"])->send();
            return false;
        }

        if (!empty($module->models) && $action->id === 'empty') {
            Yii::$app->response->redirect(["{$module->id}/{$this->id}/index"])->send();
            return false;
        }

        return true;
    }

    /**
     * @param null|string $hash
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionIndex($hash = null)
    {
        $entity = $this->findEntityByHash($hash, true);
//dump($entity);
        $fieldModels = CustomField::findByModelClass($entity['class']);

        $dataProvider = new ArrayDataProvider([
            'modelClass' => CustomField::class,
            'key' => 'id',
            'allModels' => $fieldModels,

            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('index', [
            'entity' => $entity,
            'entityList' => $this->module->models,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionEmpty()
    {
        return $this->render('empty');
    }

    /**
     * @param null $hash
     * @return string|Response
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionCreate($hash = null)
    {
        $entity = $this->findEntityByHash($hash);

        $model = new CustomField();
        $model->model_class = $entity['class'];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'hash' => $hash]);
        }

        return $this->render('form', [
            'entity' => $entity,
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $entity = $this->findEntityByClass($model->model_class);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'hash' => md5($entity['class'])]);
        }

        return $this->render('form', [
            'entity' => $entity,
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $entity = $this->findEntityByClass($model->model_class);
        $model->delete();

        return $this->redirect(['index', 'hash' => md5($entity['class'])]);
    }

    /**
     * @param $id
     * @return CustomField
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = CustomField::findOne($id);

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @param $class
     * @return mixed
     * @throws \Exception
     */
    protected function findEntityByClass($class)
    {
        return $this->findEntityByHash(md5($class));
    }

    /**
     * @param $class
     * @return mixed
     * @throws \Exception
     */
    protected function findEntityByHash($hash, $first = false)
    {
        $entity = ArrayHelper::getValue($this->module->models, $hash);

//        dump($this->module->models, 0);
//        dump($hash, 0);
//        dump($entity);

        if ($entity === null) {
            if ($first === false) {
                throw new InvalidArgumentException("Can't find entity");
            } else {
                $entity = $this->module->models[$hash] ?? $this->module->models[array_key_first($this->module->models)];
            }
        }

        return $entity;
    }

    /**
     * @param null|string $hash
     * @return array
     */
    protected function findEntity($hash = null)
    {
        return $this->module->models[$hash] ?? $this->module->models[array_key_first($this->module->models)];
    }

}