<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%custom_field}}`.
 */
class m210415_215704_create_custom_field_table extends Migration
{
    private $_table = '{{%custom_field}}';


    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_table, [
            'id' => $this->primaryKey()->comment('ID'),
            'model_class' => $this->string()->notNull()->comment('Модель'),
            'attribute' => $this->string()->null()->comment('Атрибут'),
            'label' => $this->string()->null()->comment('Label'),
            'hint' => $this->string()->null()->comment('Подсказка'),
            'placeholder' => $this->string()->null()->comment('Placeholder'),
            'description' => $this->string()->null()->comment('Описание'),
            'type' => $this->integer()->null()->comment('Тип'),
            'default' => $this->string()->null()->comment('Значение по умолчанию'),
            'required' => $this->boolean()->defaultValue(false)->comment('Обязательное'),
            'visible' => $this->boolean()->defaultValue(true)->comment('Отображать'),
            'sort' => $this->integer()->defaultValue(0),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createIndex('model_class_idx', $this->_table, 'model_class');
        $this->createIndex('type_idx', $this->_table, 'type');
        $this->createIndex('required_idx', $this->_table, 'required');
        $this->createIndex('visible_idx', $this->_table, 'visible');
        $this->createIndex('sort_idx', $this->_table, 'sort');
        $this->createIndex('is_deleted_idx', $this->_table, 'is_deleted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
