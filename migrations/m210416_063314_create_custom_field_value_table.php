<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%custom_field_value}}`.
 */
class m210416_063314_create_custom_field_value_table extends Migration
{
    private $_table = '{{%custom_field_value}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_table, [
            'id' => $this->primaryKey(),
            'field_id' => $this->integer()->notNull(),
            'model_pk' => $this->integer()->notNull(),
            'value' => $this->string()->null(),
        ], $tableOptions);

        $this->createIndex('field_idx', $this->_table, 'field_id');
        $this->createIndex('model_idx', $this->_table, 'model_pk');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
