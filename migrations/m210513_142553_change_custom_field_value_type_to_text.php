<?php

use yii\db\Migration;

/**
 * Class m210513_142553_change_custom_field_value_type_to_text
 */
class m210513_142553_change_custom_field_value_type_to_text extends Migration
{
    private $_table = '{{%custom_field_value}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn($this->_table, 'value', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn($this->_table, 'value', $this->string()->null());
    }
}
