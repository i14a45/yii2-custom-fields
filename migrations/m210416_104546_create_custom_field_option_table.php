<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%custom_field_option}}`.
 */
class m210416_104546_create_custom_field_option_table extends Migration
{
    private $_table = '{{%custom_field_option}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_table, [
            'id' => $this->primaryKey(),
            'field_id' => $this->integer()->notNull(),
            'key' => $this->string()->notNull(),
            'value' => $this->string()->null(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('fields_idx', $this->_table, 'field_id');
        $this->createIndex('key_idx', $this->_table, 'key');
        $this->createIndex('sort_idx', $this->_table, 'sort');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
