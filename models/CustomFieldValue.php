<?php

namespace i14a45\customfields\models;

use Yii;

/**
 * This is the model class for table "custom_field_value".
 *
 * @property int $id
 * @property int $field_id
 * @property int $model_pk
 * @property string|null $value
 */
class CustomFieldValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%custom_field_value}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['field_id', 'model_pk'], 'required'],
            [['field_id', 'model_pk'], 'integer'],
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'field_id' => 'Field ID',
            'model_pk' => 'Model PK',
            'option_id' => 'Option ID',
            'value' => 'Value',
        ];
    }

    /**
     * @param array $ids
     * @param mixed $modelPk
     * @return array|\yii\db\ActiveRecord[]|self[]
     */
    public static function findByFieldIds(array $ids, $modelPk)
    {
        return static::find()
            ->where([
                'field_id' => $ids,
                'model_pk' => $modelPk,
            ])
            ->indexBy('field_id')
            ->all();
    }

    /**
     * @param array $ids
     * @param mixed $modelPk
     * @return int
     */
    public static function deleteByFieldIds(array $ids, $modelPk)
    {
        return static::deleteAll([
            'field_id' => $ids,
            'model_pk' => $modelPk,
        ]);
    }
}
