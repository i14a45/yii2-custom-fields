<?php

namespace i14a45\customfields\models;

use yii2tech\ar\position\PositionBehavior;

/**
 * This is the model class for table "custom_field".
 *
 * @property int $id ID
 * @property string $model_class Модель
 * @property string $attribute
 * @property string $label Label
 * @property string $hint Подсказка
 * @property string $placeholder Placeholder
 * @property string $description Описание
 * @property int|null $type Тип
 * @property string $default Значение по-умолчанию
 * @property int|null $required Обязательное
 * @property int|null $visible Отображать
 * @property int|null $sort
 * @property int|null $is_deleted
 */
class CustomField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%custom_field}}';
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => PositionBehavior::class,
                'positionAttribute' => 'sort',
                'groupAttributes' => ['model_class'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_class', 'label'], 'required'],
            [['type'], 'integer'],
            [['required', 'visible'], 'boolean'],
            [['required'], 'default', 'value' => false],
            [['visible'], 'default', 'value' => true],
            [['attribute'], 'match', 'pattern' => '/^[a-z]+(?:-[a-z0-9]+)*$/', 'message' => 'Только латинские буквы, цифры и дефис'],
            [['attribute'], 'unique', 'targetAttribute' => 'model_class'],
            [['model_class', 'attribute', 'label', 'hint', 'placeholder', 'default', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_class' => 'Модель',
            'attribute' => 'Атрибут',
            'label' => 'Метка (label)',
            'hint' => 'Подсказка (hint)',
            'placeholder' => 'Плейсхолдер',
            'description' => 'Описание',
            'type' => 'Тип',
            'default' => 'Значение по-умолчанию',
            'required' => 'Обязательное',
            'visible' => 'Отображать',
            'sort' => 'Sort',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeHints()
    {
        return [
            'attribute' => 'Латинские буквы, цифры и дефис',
        ];
    }

    /**
     * @param $class
     * @return array|\yii\db\ActiveRecord[]|self[]
     */
    public static function findByModelClass($class)
    {
        return static::find()
            ->where(['model_class' => $class])
            ->orderBy(['sort' => SORT_ASC])
            ->indexBy('id')
            ->all();
    }

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return !empty($this->attribute) ? $this->attribute : "field_{$this->id}";
    }
}
