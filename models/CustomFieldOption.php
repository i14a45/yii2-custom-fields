<?php

namespace i14a45\customfields\models;

use Yii;

/**
 * This is the model class for table "custom_field_option".
 *
 * @property int $id
 * @property int $field_id
 * @property string $key
 * @property string|null $value
 * @property int|null $sort
 */
class CustomFieldOption extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%custom_field_option}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['field_id', 'key'], 'required'],
            [['field_id'], 'integer'],
            [['key', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'field_id' => 'Field ID',
            'key' => 'Key',
            'value' => 'Value',
            'sort' => 'Sort',
        ];
    }
}
